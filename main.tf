terraform {
  backend "http" {}
}

module "sg" {
  source  = "gitlab.com/zip-chanko/tf-aws-module-security-group/aws"
  version = "0.0.1"

  name   = "gitlab-sg"
  vpc_id = "vpc-86785de1"

  allow_rules = {
    "rule_1" = {
      ip_range    = ["192.168.0.0/24", "10.112.0.0/16"]
      port_number = 0
      protocol    = "-1"
    },
    "rule_2" = {
      port_number    = 0
      protocol       = "-1"
      security_group = "sg-f047be8d"
    }
  }
}

provider "aws" {
  region = "ap-southeast-2"
}
